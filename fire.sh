#!/bin/sh

#Firewall Feliz

#Limpa regras existentes
iptables -F

#Resolve loopback
iptables -A INPUT -i lo -j ACCEPT

#Libera SSH para o mundo
iptables -A INUT -p tcp --dport 22 -j ACCEPT

#Bloqueia tudo
iptables -A INPUT -p tcp --syn -j DROP
